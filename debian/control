Source: amavisd-milter
Section: mail
Priority: optional
Maintainer: Harald Jenny <harald@a-little-linux-box.at>
Build-Depends: debhelper-compat (= 12), libmilter-dev, pandoc
Homepage: https://github.com/prehor/amavisd-milter
Vcs-Browser: https://salsa.debian.org/haraldj-guest/amavisd-milter
Vcs-Git: https://salsa.debian.org/haraldj-guest/amavisd-milter.git
Standards-Version: 4.5.1
Rules-Requires-Root: no

Package: amavisd-milter
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, amavisd-new, lsb-base
Pre-Depends: ${misc:Pre-Depends}
Recommends: sendmail | postfix
Provides: amavisd-new-milter
Breaks: amavisd-new-milter (<= 1:2.6.4-2)
Replaces: amavisd-new-milter (<= 1:2.6.4-2)
Description: amavisd-new interface for milter-capable MTAs
 This package provides a milter for amavisd-new that works with
 Sendmail or Postfix, using the AM.PDP protocol.
 .
 Replacing the older amavisd-new-milter program, amavisd-milter makes
 use of the full functionality of amavisd-new. It supports using spam
 and virus information header fields, rewriting message subjects,
 adding address extensions, and selectively removing recipients.
